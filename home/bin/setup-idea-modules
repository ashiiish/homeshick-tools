#!/usr/bin/env python

import sys
import os
import glob
from lxml.etree import ElementTree as ET
from lxml.etree import Element as E
from lxml import etree
import yaml


PYTHON_SDK_TYPE = 'Python SDK'
PRODUCT_LIST = ["CLion", "DataGrip", "IntelliJ", "IdeaIC", "RubyMine", "PhpStorm",
                "WebStorm", "AndroidStudio", "PyCharm", "GoLand", "Rider", "AppCode"]
OS_NAME = os.getenv("OS")
HOME = os.getenv("HOME")


def get_ides():
    if OS_NAME == "Darwin":
        template_str = HOME + "/Library/Preferences/{0}*"
    else:
        template_str = HOME + "/.{0}*/config"

    ides_list = [glob.glob(template_str.format(ide)) for ide in PRODUCT_LIST]
    ides = [ide for ide_list in ides_list for ide in ide_list]

    return ides


def get_jdks(config_home):
    config_file = os.path.join(config_home, 'options/jdk.table.xml')
    config = etree.parse(config_file)
    jdks = [{
        'name': j.find('name').get('value'),
        'type': j.find('type').get('value'),
    } for j in config.iter('jdk')]

    return jdks


def get_python_jdks(config_home):
    return [j for j in get_jdks(config_home) if j['type'] == PYTHON_SDK_TYPE]


def py2element(name: str, attributes: dict = None):
    if attributes is not None and len(attributes.keys()) > 0:
        element = E(name)
        for attribute_name in attributes.keys():
            element.set(attribute_name, attributes[attribute_name])
    else:
        element = E(name)

    return element


def add_module(module_dir, module_name, jdk_name):
    module_filename = ".idea/modules/{}.iml".format(module_name)
    if os.path.isfile(module_filename):
        print("Skipping module: {}...".format(module_name))
        return
    else:
        print("Setting up module: {}...".format(module_name))

    # Create module config
    content_source_root = py2element('content', {
        'url': 'file://$MODULE_DIR$/../../{}'.format(module_dir)
    })
    content_source_root.append(py2element(
        'sourceFolder', {
            'url': 'file://$MODULE_DIR$/../../{}/{}'
                .format(module_dir, module_name),
            'isTestSource': 'false'
        }
    ))
    content_source_root.append(py2element(
        'sourceFolder', {
            'url': 'file://$MODULE_DIR$/../../{}/test'.format(module_dir),
            'isTestSource': 'true'
        }
    ))

    component_doc_root = py2element('component', {
        "name": "NewModuleRootManager",
        "inherit-compiler-output": "true",
    })
    component_doc_root.append(py2element('exclude-output'))
    component_doc_root.append(content_source_root)
    component_doc_root.append(py2element('orderEntry', {
        'type': 'jdk',
        'jdkName': jdk_name,
        'jdkType': PYTHON_SDK_TYPE,
    }))
    component_doc_root.append(py2element('orderEntry', {
        'type': 'sourceFolder',
        'forTests': 'false',
    }))

    module_doc_root = py2element('module', {
        'type': 'PYTHON_MODULE',
        'version': '4'
    })
    module_doc_root.append(component_doc_root)

    module_config = ET(module_doc_root)
    module_config.write(module_filename,
                        encoding="utf-8", xml_declaration=True,
                        pretty_print=True)

    # Add the module to the project
    module_fileurl = "file://$PROJECT_DIR$/{}".format(module_filename)
    module_filepath = "$PROJECT_DIR$/{}".format(module_filename)
    project_config_file = '.idea/modules.xml'
    project_config = etree.parse(project_config_file)
    project_modules = project_config.find('.//modules')
    project_modules.append(py2element('module', {
        'fileurl': module_fileurl,
        'filepath': module_filepath,
    }))
    project_config.write(project_config_file,
                         encoding="utf-8", xml_declaration=True,
                         pretty_print=True)


def help_msg():
    print("""
Config file name: idea-modules.yml
Expected format:
- name: event_to_parquet
  dir: event_to_parquet
  sdk: Python 3.6 (ds)
  type: python
          """)


if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == '-h':
        help_msg()
        sys.exit(0)

    print("Installed IDEs:")
    ides = get_ides()
    for ide, index in zip(ides, range(1, len(ides) + 1)):
        print("  {}. {}".format(index, ide))

    config_home_index = int(input('Please choose a Jetbrains config location: ')) - 1
    config_home = ides[config_home_index]
    print("Using config location: {}".format(config_home))

    python_sdks = get_python_jdks(config_home)
    sdks = {}

    print("\nConfigured python SDKs...")
    if len(python_sdks) > 0:
        for sdk, index in zip(python_sdks, range(1, len(python_sdks) + 1)):
            print("  {}: {} [{}]".format(index, sdk['name'], sdk['type']))

        python_sdk_index = int(input("Please choose a python SDK: ")) - 1
        sdks['python'] = python_sdks[python_sdk_index]['name']
        print("Using python sdk: {}".format(sdks['python']))
    else:
        print("Please install a python sdk first in {}".format(config_home.split("/")[-1]))
        sys.exit(1)

    # Load config from yaml: idea-modules.yml
    # Expected
    # - name: event_to_parquet
    #   dir: event_to_parquet
    #   sdk: Python 3.6 (ds)
    #   type: python
    print("\nLoading config from `idea-modules.yml`...")
    modules = yaml.load(open('idea-modules.yml'))

    for module in modules:
        if module['type'] == 'python':
            if sdks.get('python') is not None:
                add_module(module['dir'], module['name'], sdks['python'])
            else:
                add_module(module['dir'], module['name'], module['sdk'])
        else:
          print("Ignoring module: {} of type: {}".format(module['name'], module['type']))

    print("Done.")
